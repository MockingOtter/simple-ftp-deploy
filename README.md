# Simple FTP Deploy

Simple deployment script of websites using ftp

include php linting


## Getting started

* Put the  .gitlab-ci.yml  at the root of your Gitlab repository

* Register the following variables ( You can use those variable globally or per project):
    
    - PHPVERSION : (str) like 8.1 or 7.4 or latest. it will  be used to choose php image for linting.
    - FTP_HOST : (str) Server fqdn or ip.
    - FTP_USER : (str) ftp username
    - FTP_PASSWORD : (str) ftp password
    - LOCAL_WEBROOT :  (str) Website root directoey in the repository 
    - REMOTE_WEBROOT : (str) Website root directory on the server
    - EXCLUDED_FILES : (str) Array of regex. E.g. "'^conf/' '^README.md'"
    

> Uncomment "#when: manual" if you wan't to manually trigger pipeline
